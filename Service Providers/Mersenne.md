Centre Mersenne is supported by CNRS and Universite Grenoble-Alpes where it is hosted. It can offer (for free, but capacity may be limited) a customized OJS editorial software with IT support, DOIs, plagiarism checking, with other services such as typesetting offered a la carte. 

More details:

* [Mersenne site] (https://www.centre-mersenne.org/)
* [Algebraic Combinatorics] (http://algebraic-combinatorics.org/)

The Centre is relatively new but the team behind it has years of experience running
well-known projects such as [Numdam](http://www.numdam.org/) and [Cedram](http://www.cedram.org/?lang=en).
In particular, Cedram has been handling several established journals, such as [Annales de l’institut Fourier](http://aif.cedram.org/?lang=en).

Their approach is very flexible and can be custom cut for the journal's needs.
