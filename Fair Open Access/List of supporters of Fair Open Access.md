The people listed below all publicly support the Fair Open Access Principles.

Mathematics
-----------

* Beck, Matthias - San Francisco State University
* Bressoud, David - Macalester College
* Chafaï,  Djalil -  Université Paris-Dauphine
* Cohn, Henry -  Microsoft Research
* Gee, Toby -  Imperial College London
* Gowers, Timothy - University of Cambridge
* Grötschel, Martin - Berlin-Brandenburgische Akademie der Wissenschaften
* Hales, Thomas -  University of Pittsburgh
* Joyal, André - Université du Québec à Montréal
* Kuperberg, Krystyna - Auburn University
* Mumford, David - Brown University
* Odlyzko, Andrew - University of Minnesota
* Reiner, Victor - University of Minnesota