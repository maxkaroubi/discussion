Why convert my journal to Fair Open Access?
======

* Help your authors, [especially junior ones](https://gitlab.com/publishing-reform/discussion/issues/17), 
 with better promotion and dissemination of their work, leading to better opportunities in their careers.
* Help the research community to reclaim control of the journal. 
This will allow to choose services from best providers, 
[superior](https://gitlab.com/publishing-reform/discussion/issues/32) 
and [less confusing](https://gitlab.com/publishing-reform/discussion/issues/29)
than what is provided by unincentivised publishers lacking competition.
Superior journal quality is guaranteed through community invited editors,
rather than hired by publishers' representatives having no subject expertise.
This opens the way to future innovations and improvements in journal processes.
* All papers published in your journal will immediately be freely available to anyone with 
an Internet connection: no paywalls, no embargo periods, no discrimination against readers in poorer countries. 
In particular, the public, who in most case subsidize the research, 
will be able to [access the results] (https://whoneedsaccess.org/).
* The impact of your journal and the benefits for the authors will increase:  open access journal articles [attract higher citation rates] (https://sparceurope.org/what-we-do/open-access/sparc-europe-open-access-resources/open-access-citation-advantage-service-oaca/) and the publicity attending the journal conversion will help the journal's profile. 
* Mathematics [journals that declared independence](http://oad.simmons.edu/oadwiki/Journal_declarations_of_independence) from large commercial publishers [do better than their original versions](https://mcw.blogs.auckland.ac.nz/2016/10/08/what-happens-to-journals-that-break-away/).
* Libraries will move closer to being able to cancel exorbitant subscriptions, 
freeing up money for the more deserving research community benefits rathter than donating 
to shareholders of high-profit commercial companies.
* Your journal and its quality will not be negatively impacted
by [large scale cancellations](https://www.the-scientist.com/?articles.view/articleNo/52208/title/French-Universities-Cancel-Subscriptions-to-Springer-Journals/).



